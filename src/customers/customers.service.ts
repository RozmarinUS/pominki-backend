import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Customers } from './customers.entity';
import { CreateCustomerDto } from './dto/create-customer.dto';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customers)
    public customersRepository: Repository<Customers>,
  ) {}

  /**
   * Получить всех клиентов
   */
  getAll() {
    return this.customersRepository.find();
  }

  /**
   * Получить клиента по ID
   * @param id
   */
  getById(id: number) {
    return this.customersRepository.findOne({ id });
  }

  /**
   * Получть клиента по email
   * @param email
   */
  getByEmail(email: string) {
    return this.customersRepository.findOne({ email });
  }

  /**
   * Создать нового клиента
   * @param data
   */
  async create(data: CreateCustomerDto) {
    return this.customersRepository.insert(data);
  }
}
