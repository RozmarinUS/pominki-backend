import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Orders, OrdersDishes } from './orders.entity';
import { Customers } from '../customers/customers.entity';
import { Dishes } from '../dishes/dishes.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Orders)
    public ordersRepository: Repository<Orders>,
    @InjectRepository(OrdersDishes)
    public ordersDishesRepository: Repository<OrdersDishes>,
    @InjectRepository(Dishes)
    public dishesRepository: Repository<Dishes>,
  ) {}
  find() {
    return this.ordersRepository
      .createQueryBuilder('orders')
      .leftJoinAndSelect(
        'orders.status',
        'statuses',
        'orders.statusId = statuses.id',
      )
      .leftJoinAndSelect(
        'orders.customer',
        'customers',
        'orders.customerId = customers.id',
      )
      .select([
        'orders.id',
        'statuses.id',
        'statuses.name',
        'customers.id',
        'customers.email',
        'customers.phone',
        'customers.first_name',
        'customers.last_name',
        'orders.createdAt',
      ]);
  }
  async findAll() {
    const arr = [];
    for (const order of await this.find().getMany()) {
      const dishes = [];
      for (const item of await this.ordersDishesRepository
        .createQueryBuilder('orders_dishes')
        .leftJoinAndSelect(
          'orders_dishes.dish',
          'dishes',
          'dishes.id = orders_dishes.dish',
        )
        .where({ order: order.id })
        .getMany()) {
        if (item.dish instanceof Dishes)
          dishes.push({
            id: item.dish.id,
            name: item.dish.name,
            image: item.dish.image,
            price: item.dish.price,
            weight: item.dish.weight,
            size: item.dish.size,
          });
      }
      arr.push({
        id: order.id,
        status: order.status,
        customer: order.customer,
        createdAt: order.createdAt,
        dishes,
      });
    }
    return arr;
  }
  getById(id: number) {
    return this.dishesRepository.findOne({ id });
  }
  async checkDishes(dishes: number[]) {
    for (const id of dishes) {
      const dish = await this.dishesRepository.findOne({ id });
      if (!dish) return false;
    }
    return true;
  }

  async create(data: { dishes: number[]; date: Date }, customer: Customers) {
    const order = await this.ordersRepository.insert({
      createdAt: new Date(),
      customer: customer.id,
      status: 'ordered',
    });
    for (const dishId of data.dishes) {
      await this.ordersDishesRepository.insert({
        dish: dishId,
        order: order.raw.insertId,
      });
    }
  }
}
