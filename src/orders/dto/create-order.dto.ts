import { Customers } from '../../customers/customers.entity';

export class CreateOrderDto {
  public dishes: number[];
  public date: Date;
  public customer: Customers;
}
