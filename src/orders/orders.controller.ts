import {
  BadRequestException,
  Body,
  Controller,
  Get,
  NotFoundException,
  Post,
  Put,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { OrdersService } from './orders.service';
import { TransformInterceptor } from '../transform.interceptor';
import { CreateOrderDto } from './dto/create-order.dto';
import { CustomersService } from '../customers/customers.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { UpdateOrderDto } from './dto/update-order.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('orders')
@Controller('orders')
@UseInterceptors(TransformInterceptor)
export class OrdersController {
  constructor(
    private readonly ordersService: OrdersService,
    private readonly customersService: CustomersService, //  private readonly customersService: CustomersService,
  ) {}

  @Get()
  @UseGuards(JwtAuthGuard)
  getAll() {
    return this.ordersService.findAll();
  }

  @Put(':id')
  async edit(@Body() data: UpdateOrderDto) {
    if (data.action === 'dishes') {
    } else {
      throw new BadRequestException('Action is not specified or wrong');
    }
  }

  @Post()
  async create(@Body() data: CreateOrderDto) {
    const { dishes, date } = data;
    let customer = await this.customersService.getByEmail(data.customer.email);
    if (!customer) {
      await this.customersService.create({
        ...data.customer,
        createdAt: new Date(),
      });
    }
    customer = await this.customersService.getByEmail(data.customer.email);
    const checkDish = await this.ordersService.checkDishes(dishes);

    if (checkDish) {
      await this.ordersService.create({ dishes, date }, customer);
    } else {
      throw new NotFoundException('One or several dishes was not found');
    }
    // await this.ordersService.create({ dishes, date }, customer);
  }
}
