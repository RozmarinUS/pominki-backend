import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

export enum usersRoles {
  manager = 'manager',
  operator = 'operator',
}

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id: string | number;

  @Column({ nullable: false, unique: true })
  email: string;

  @Column({ nullable: false })
  password: string;

  @Column({ type: 'enum', enum: usersRoles, default: usersRoles.manager })
  role: usersRoles;
}
