import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  UseInterceptors,
  NotFoundException,
  UploadedFile,
  BadRequestException,
} from '@nestjs/common';
import { DishesService } from './dishes.service';
import { TransformInterceptor } from '../transform.interceptor';
import { CreateDishDto } from './dto/create-dish.dto';
import { UpdateDishDto } from './dto/update-dish.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';

@ApiTags('dishes')
@Controller('dishes')
@UseInterceptors(TransformInterceptor)
export class DishesController {
  constructor(private readonly dishesService: DishesService) {}

  @ApiOperation({ summary: 'Получение всех блюд или по категории' })
  @ApiQuery({ name: '' })
  @Get()
  getAll(@Query() query) {
    if (query.groupBy === 'category') {
      return this.dishesService.getAllByCategory(query.id);
    } else {
      return this.dishesService.getAll();
    }
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  getOne(@Param('id') id) {
    return this.dishesService.getById(id);
  }

  @Post()
  @UseGuards(JwtAuthGuard)
  create(@Body() data: CreateDishDto) {
    return this.dishesService.create(data);
  }

  @Post(':id/upload')
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(
    FileInterceptor('image', {
      storage: diskStorage({
        destination: './uploads/dishes',
        filename(req, file, cb) {
          cb(null, `${req.params.id}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  async uploadFile(@Param('id') id, @UploadedFile() file: Express.Multer.File) {
    if (!file) throw new BadRequestException('No image file');
    if (await this.dishesService.getById(id)) {
      console.log(file);
      return this.dishesService.handleUpload(
        id,
        '/' + file.path.replace(/\\+/g, '/'),
      );
    } else {
      throw new NotFoundException('Dish not found');
    }
  }

  @Put(':id')
  @UseGuards(JwtAuthGuard)
  async update(@Param('id') id, @Body() data: UpdateDishDto) {
    const model = await this.dishesService.getById(id);
    if (model) {
      await this.dishesService.update(id, data);
    } else {
      throw new NotFoundException('Dish not found');
    }
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  async delete(@Param('id') id) {
    const model = await this.dishesService.getById(id);
    if (model) {
      await this.dishesService.remove(id);
    } else {
      throw new NotFoundException('Dish not found');
    }
  }
}
