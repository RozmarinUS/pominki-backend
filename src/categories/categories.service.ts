import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Categories } from './categories.entity';
import { Repository } from 'typeorm';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { Dishes } from '../dishes/dishes.entity';
import { OrdersDishes } from '../orders/orders.entity';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(Categories)
    private categoriesRepository: Repository<Categories>,
    @InjectRepository(Dishes)
    private dishesRepository: Repository<Dishes>,
    @InjectRepository(OrdersDishes)
    private ordersDishesRepository: Repository<OrdersDishes>,
  ) {}

  /**
   * Получить все категории
   */
  getAll(): Promise<Categories[]> {
    return this.categoriesRepository.find({
      select: ['id', 'name', 'image'],
    });
  }

  /**
   * Получить категорию по ID
   * @param id
   */
  getById(id) {
    return this.categoriesRepository.findOne(
      {
        id,
      },
      {
        select: ['id', 'name', 'image'],
      },
    );
  }

  /**
   * Создание новой категории
   * @param data
   */
  async create(data: CreateCategoryDto) {
    return this.categoriesRepository.insert(data);
  }

  /**
   * Обновление категории по ID
   * @param id
   * @param data
   */
  async update(id, data: UpdateCategoryDto) {
    await this.categoriesRepository.update({ id }, data);
    return this.getById(id);
  }

  /**
   * Удаление категории по ID, очистка блюд в заказах,
   * и удаление блюд с указанной категорией
   * @param id
   */
  async remove(id: number) {
    const dishes = await this.dishesRepository.find({
      where: {
        category: id,
      },
    });
    for (const dish of dishes) {
      await this.ordersDishesRepository.delete({ dish: dish.id });
    }
    await this.dishesRepository.delete({ category: id });
    return this.categoriesRepository.delete({ id });
  }

  /**
   * Обработка фотографии
   * @param id
   * @param file
   */
  async handleUpload(id: number, file) {
    await this.categoriesRepository.update({ id }, { image: file });
    return;
  }
}
