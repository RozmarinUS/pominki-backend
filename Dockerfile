FROM node:10-alpine
WORKDIR /usr/src/app

COPY . .
RUN npm ci

EXPOSE 8088

CMD ["npm", "run", "start"]
